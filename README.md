# What is vagrant-2elasticsearch-vbox ?
It is a [Vagrant](http://www.vagrantup.com/) setup I did to try out ElasticSearch clustering and replication.
It runs 2 VMs (*lemon* and *orange*), each with a ES instance communicated through a private virtual box network.

Both ES instances discover each other via Zen Multicast Discovery.

# How easy is to use vagrant-2elasticsearch-vbox ?
Supereasy. For example...

### Clone the repo and fetch the modules

    $ git clone git@bitbucket.org:pfigue/vagrant-2elasticsearch-vbox.git
    $ cd vagrant-2elasticsearch-vbox/
    $ git submodule init
    Submodule 'puppet/modules/wget' (https://github.com/maestrodev/puppet-wget.git) registered for path 'puppet/modules/wget'
    $ git submodule update
    Cloning into 'puppet/modules/wget'...
    remote: Counting objects: 384, done.
    remote: Compressing objects: 100% (195/195), done.
    remote: Total 384 (delta 142), reused 384 (delta 142)
    Receiving objects: 100% (384/384), 57.22 KiB, done.
    Resolving deltas: 100% (142/142), done.
    Submodule path 'puppet/modules/wget': checked out 'b64842ae6e3a1e070d9536fda37e05aca968a0d3'

### Setup the machine
    $ vagrant up
    Bringing machine 'lemon' up with 'virtualbox' provider...
    Bringing machine 'orange' up with 'virtualbox' provider...
    [lemon] Importing base box 'precise64'...
    [lemon] Matching MAC address for NAT networking...
    [lemon] Setting the name of the VM...
    [lemon] Clearing any previously set forwarded ports...
    [lemon] Clearing any previously set network interfaces...
    [lemon] Preparing network interfaces based on configuration...
    [lemon] Forwarding ports...
    [lemon] -- 22 => 2222 (adapter 1)
    [lemon] Booting VM...
    [lemon] Waiting for machine to boot. This may take a few minutes...
    [lemon] Machine booted and ready!
    [lemon] Setting hostname...
    [lemon] Configuring and enabling network interfaces...
    [lemon] Mounting shared folders...
    [lemon] -- /vagrant
    [lemon] -- /tmp/vagrant-puppet-1/manifests
    [lemon] -- /tmp/vagrant-puppet-1/modules-0
    [lemon] Running provisioner: puppet...
    Running Puppet with site.pp...
    stdin: is not a tty
    notice: /Stage[main]//Exec[apt-get update]/returns: executed successfully
    notice: /Stage[main]//Package[openjdk-7-jre-headless]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[jq]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[curl]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[tmux]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[vim]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Wget::Fetch[Download ElasticSearch 1.1.0 Debian Package]/Exec[wget-Download ElasticSearch 1.1.0 Debian Package]/returns: executed successfully
    notice: /Stage[main]//Exec[install ES]/returns: executed successfully
    notice: /Stage[main]//File[/etc/init.d/elasticsearch]/content: content changed '{md5}130a053aa4c18212bceede4ca081314f' to '{md5}31579a67046c3b01d57e3db4a91c9ff9'
    notice: /Stage[main]//File[/etc/elasticsearch/elasticsearch.yml]/content: content changed '{md5}e0ea794da86d89b7e1b8192504349313' to '{md5}13e1f958d2de2a764ed36525faabdf8b'
    notice: /Stage[main]//Service[elasticsearch]/ensure: ensure changed 'stopped' to 'running'
    notice: /Stage[main]//Service[elasticsearch]: Triggered 'refresh' from 3 events
    notice: Finished catalog run in 265.59 seconds
    [orange] Importing base box 'precise64'...
    [orange] Matching MAC address for NAT networking...
    [orange] Setting the name of the VM...
    [orange] Clearing any previously set forwarded ports...
    [orange] Fixed port collision for 22 => 2222. Now on port 2200.
    [orange] Clearing any previously set network interfaces...
    [orange] Preparing network interfaces based on configuration...
    [orange] Forwarding ports...
    [orange] -- 22 => 2200 (adapter 1)
    [orange] Booting VM...
    [orange] Waiting for machine to boot. This may take a few minutes...
    [orange] Machine booted and ready!
    [orange] Setting hostname...
    [orange] Configuring and enabling network interfaces...
    [orange] Mounting shared folders...
    [orange] -- /vagrant
    [orange] -- /tmp/vagrant-puppet-2/manifests
    [orange] -- /tmp/vagrant-puppet-2/modules-0
    [orange] Running provisioner: puppet...
    Running Puppet with site.pp...
    stdin: is not a tty
    notice: /Stage[main]//Exec[apt-get update]/returns: executed successfully
    notice: /Stage[main]//Package[openjdk-7-jre-headless]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[jq]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[curl]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[tmux]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[vim]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Exec[install ES]/returns: executed successfully
    notice: /Stage[main]//File[/etc/init.d/elasticsearch]/content: content changed '{md5}130a053aa4c18212bceede4ca081314f' to '{md5}31579a67046c3b01d57e3db4a91c9ff9'
    notice: /Stage[main]//File[/etc/elasticsearch/elasticsearch.yml]/content: content changed '{md5}e0ea794da86d89b7e1b8192504349313' to '{md5}6a3ad7aa2525af0782afcc6d7666ed94'
    notice: /Stage[main]//Service[elasticsearch]/ensure: ensure changed 'stopped' to 'running'
    notice: /Stage[main]//Service[elasticsearch]: Triggered 'refresh' from 3 events
    notice: Finished catalog run in 77.59 seconds
    $


## Test it

Checking how many nodes has the cluster. You may need to use another IP if you changed it in the Vagrantfile. The answer should be the same if you ask one ES instance, or the other.

    $ curl -s -XGET "http://192.168.50.3:9200/_cluster/health?pretty=true" | jq '.'
    {
        "unassigned_shards": 0,
        "initializing_shards": 0,
        "cluster_name": "grocery",
        "status": "green",
        "timed_out": false,
        "number_of_nodes": 2,
        "number_of_data_nodes": 2,
        "active_primary_shards": 0,
        "active_shards": 0,
        "relocating_shards": 0
    }
    $

Show the names of the cluster members. Same answer from both nodes:

    $ curl -s -XGET 'http://192.168.50.4:9200/_nodes' | jq '.nodes[].name'
    "lemon"
    "orange"
    $


Requesting a non existent document:

    $ curl -s 'http://192.168.50.4:9200/twitter/tweet/1' | jq '.'                                                                                                                                {
            "status": 404,
            "error": "IndexMissingException[[twitter] missing]"
        }

Creating that document in one server (*192.168.50.4*):

    $ curl -s -XPUT 'http://192.168.50.4:9200/twitter/tweet/1' -d '{                                                                                                                    
        > "user" : "kimchy",
        > "post_date" : "2009-11-15T14:12:12",
        > "message" : "trying out Elasticsearch"
        > }' | jq '.'
    {
    "created": true,
    "_version": 1,
    "_id": "1",
    "_type": "tweet",
    "_index": "twitter"
    }

Asking for that document in the other server of the cluster (*192.168.50.3*):

    $ curl -s 'http://192.168.50.3:9200/twitter/tweet/1' | jq '.'                                                                                                                                
    {
        "_source": {
            "message": "trying out Elasticsearch",
            "post_date": "2009-11-15T14:12:12",
            "user": "kimchy"
        },
        "found": true,
        "_version": 1,
        "_id": "1",
        "_type": "tweet",
        "_index": "twitter"
    }
    $

Asking for it in the first server (*192.168.50.4*):

    $ curl -s 'http://192.168.50.4:9200/twitter/tweet/1' | jq '.'                                                                                                                                
    {
        "_source": {
            "message": "trying out Elasticsearch",
            "post_date": "2009-11-15T14:12:12",
            "user": "kimchy"
        },
        "found": true,
        "_version": 1,
        "_id": "1",
        "_type": "tweet",
        "_index": "twitter"
    }
    $ 


# Notes and more details

+ The previous installation steps rely on several tools, like [Vagrant](http://www.vagrantup.com/) and [jq](http://stedolan.github.io/jq/).
+ It uses Virtual Box as virtualization tool.
    + LXC would be much nicer for Linux users.
+ The base box is *precise64*, so **Ubuntu 12.04 LTS amd64**.
+ Ports 9200/tcp, 9300/tcp and 9500/tcp are forwarded from host to guest.
    + Nonetheless, Thrift protocol (9500/tcp) is not enabled at the moment.
    + Java VM is listening only on IPv4 sockets, not in IPv6.
+ The puppet manifests depend on [maestrodev/wget puppet module](https://forge.puppetlabs.com/maestrodev/wget) version 1.4.1;
    + There is also an [elasticsearch/elasticsearch puppet module](https://forge.puppetlabs.com/elasticsearch/elasticsearch), which can install ES as well.
+ See also this [other easier setup](https://bitbucket.org/pfigue/vagrant-elasticsearch-vbox)

